package mobilecomputinghausarbeit17.activitytrackerfl;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Steffen on 04.12.2017.
 */

public class StepLogService extends Service implements SensorEventListener {

    private static final String DEBUG_TAG = "StepLogService";

    private SensorManager sensorManager = null;
    private Sensor sensor = null;
    private String notificationText = "";
    public DatabaseHandler db;
    SharedPreferences mySharedValues;
    //Variablen zur Verwaltung der Schritte


    public int onStartCommand(Intent intent, int flags,int startId) {
        Log.d(DEBUG_TAG,"On StartCommand");
        db = new DatabaseHandler(this);
        this.mySharedValues = PreferenceManager.getDefaultSharedPreferences(this);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //Daten aus Shared Preferences lesen
        int lastStepNumber = this.mySharedValues.getInt("lastStepNumber",(int) event.values[0]);
        Log.d(DEBUG_TAG,"oldSteps: "+lastStepNumber);

        //Steps der vergangenen Minute berechnen
        int stepsThisMinute = (int) event.values[0] - lastStepNumber;
        Log.d(DEBUG_TAG,"stepsThisMinute: "+stepsThisMinute);


        int steps1MinuteAgo = stepsThisMinute;
        int steps2MinuteAgo = this.mySharedValues.getInt("stepsTwoIntervalAgo",0);
        boolean isTrack = this.mySharedValues.getBoolean("isTrack",false);

        Log.d(DEBUG_TAG,"Steps One Minute Ago: "+steps1MinuteAgo);
        Log.d(DEBUG_TAG,"Steps Two Minute Ago " +steps2MinuteAgo);



        //Zählerstand der Schritte abspeichern.
        SharedPreferences.Editor editor = this.mySharedValues.edit();
        editor.putInt("lastStepNumber",(int)event.values[0]);
        //editor.apply();

        //Ermitteln des aktuellen Datums (Zum Abspeichern in der Datenbank
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String actualDate = dateFormat.format(date);
        DateFormat trackDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Log.d(DEBUG_TAG,"Es ist: " + trackDateFormat.format(date));


        //Code zur Klassifikation des Schrittintervals als Aktivität
        if(stepsThisMinute > 20 && stepsThisMinute < 160) {
            db.addToStatistic(actualDate,"KEY_WALK",1);
        } else if (stepsThisMinute > 160 ) {
            db.addToStatistic(actualDate,"KEY_RUN",1);
        }

        //Code zur Erkennung zusammenhängender Aktivitäten (tracks), wird 2 Minuten lang gerannt oder gejoogt, aufzeichnung einer Aktivität starten.
        //Start eines Tracks erfassen...
        if(( isTrack == false)&& (steps1MinuteAgo > 20 && steps1MinuteAgo < 160) && (steps2MinuteAgo > 20 && steps2MinuteAgo < 160)){
            //Der User scheint länger zu Gehen
            String TrackTimeStamp = trackDateFormat.format(date);
            Log.d(DEBUG_TAG,"Gehen Track gestartet: " + TrackTimeStamp);
            //SharedPreferences.Editor trackStart1Editor = this.mySharedValues.edit();
            editor.putString("mode","Gehen");
            editor.putBoolean("isTrack",true);
            editor.putString("trackStart",TrackTimeStamp);
            //trackStart1Editor.apply();
        } else if ((isTrack == false) && (steps1MinuteAgo > 160) && (steps2MinuteAgo > 160)) {
            String TrackTimeStamp = trackDateFormat.format(date);
            Log.d(DEBUG_TAG,"Joggen Track gestartet: "+TrackTimeStamp);
            //SharedPreferences.Editor trackStart2Editor = this.mySharedValues.edit();
            editor.putString("mode","Joggen");
            editor.putBoolean("isTrack",true);
            editor.putString("trackStart",TrackTimeStamp);
            //trackStart2Editor.apply();
        }

        //Abbruch eines Tracks:
        if(isTrack == true && steps1MinuteAgo < 20 ) {
            String trackEndStamp = trackDateFormat.format(date);
            Log.d(DEBUG_TAG,"Der Track wurde beendet am :"+trackEndStamp);

            String mode = this.mySharedValues.getString("mode",""); //Default Value sollte hier egal sein.
            String trackStartStamp = this.mySharedValues.getString("trackStart","");
            this.db.addTrack(actualDate,mode,trackStartStamp,trackEndStamp);

            //Ende des Tracks in den SharedPreferences markieren
            //SharedPreferences.Editor trackEndEditor = this.mySharedValues.edit();
            editor.putBoolean("isTrack",false);
            this.notificationText = "Es wurde "+mode + " automatisch erfasst";
            this.fireNotification(this.notificationText);

            //trackEndEditor.apply();
        }
        //Log.d(DEBUG_TAG,"HEUTIGE RUN WERTE: "+db.getStatData(actualDate,"KEY_RUN"));
        //Log.d(DEBUG_TAG,"HEUTIGE WALK WERTE: "+db.getStatData(actualDate,"KEY_WALK"));


        //Vergangene Schrittwerte abspeichern
        //SharedPreferences.Editor stepEditor = this.mySharedValues.edit();
        editor.putInt("stepsTwoIntervalAgo",steps1MinuteAgo);
        //stepEditor.apply();
        editor.apply();

        Log.d(DEBUG_TAG,"********************************************************************");
        sensorManager.unregisterListener(this);
        stopSelf();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //DO Nothing
    }

    //Code aus Übungsaufgabe 6 übernommen, mit Parametrisierung versehen.
    void fireNotification(String notificationText) {
        //Log.d(DEBUG_TAG, "Notification zeichnen");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setTicker("The ticker");
        builder.setContentTitle("Flensburger ActivityTracker");
        builder.setContentText(notificationText);
        //Auskommentierte Zeile weil w_small_icon_24_24 nicht vrohanden ist
        //builder.setSmallIcon(R.drawable.w_small_icon_24_24);
        builder.setSmallIcon(R.drawable.ic_launcher_background);
        // von der Notification aus den Aufruf der Activity gewaehrleisten
        // s. https://developer.android.com/guide/topics/ui/notifiers/notifications.html#Design
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);// und setzen
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify("direct_tag", 4711, builder.build());
    }
}
