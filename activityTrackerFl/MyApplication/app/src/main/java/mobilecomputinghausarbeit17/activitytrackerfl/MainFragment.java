package mobilecomputinghausarbeit17.activitytrackerfl;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Steffen on 01.01.2018.
 * modified by Mirco
 */

public class MainFragment extends Fragment implements SensorEventListener, View.OnClickListener{
    private static final String	DEBUG_TAG	= "MainFragment";

    public float steps = 0;
    public float oldCount = -1;
    public TextView stepsTextView;
    public TextView walkingTextView;
    public TextView joggingTextView;
    public SensorManager sensorManager;
    public boolean isRunning;
    public KreisDiagramm diagramm;
    public DatabaseHandler db;
    public int walkingMinutes;
    public int runningMinutes;

    public MainFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);

        stepsTextView = (TextView) view.findViewById(R.id.steps);
        walkingTextView = (TextView) view.findViewById(R.id.walking);
        joggingTextView = (TextView) view.findViewById(R.id.jogging);


        Button butChangeDate = (Button) view.findViewById(R.id.button4);
        butChangeDate.setOnClickListener(this);

        //Background Process aktivieren
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent broadcastIntent = new Intent(getActivity().getApplicationContext(), StepLogService.class );
        PendingIntent scheduledIntent = PendingIntent.getService(getActivity().getApplicationContext(), 0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, scheduledIntent);

        //Manager für die Sensoren starten
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        db = new DatabaseHandler(getActivity());

        //Kreisdiagramm anzeigen, 1. Tag ermitteln, Werte aus Datenbank holen, Kreisdiagramm zeichnen
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String actualDate = dateFormat.format(date);

        walkingMinutes = db.getStatData(actualDate,"KEY_WALK");
        runningMinutes = db.getStatData(actualDate,"KEY_RUN");

        this.diagramm = (KreisDiagramm) view.findViewById(R.id.KreisDiagramm);

        this.diagramm.drawDiagramm(runningMinutes,walkingMinutes);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isRunning = true;
        Sensor countSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            sensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Toast.makeText(getActivity(), "Sensor nicht gefunden", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isRunning = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

            db.AddStep(String.valueOf(event.values[0]));

            stepsTextView.setText(String.valueOf(db.GetSingleStep()));

            if(walkingMinutes == 0){
                walkingTextView.setText("noch nicht gelaufen");
            }
            else{
                walkingTextView.setText(String.valueOf(walkingMinutes) + " Minute(n)");
            }
            if(runningMinutes == 0){
                joggingTextView.setText("noch nicht gelaufen");
            } else{
                joggingTextView.setText(String.valueOf(runningMinutes) + " Minuten(n)");
            }
            this.diagramm.drawDiagramm(runningMinutes,walkingMinutes);
        }

    //Muss rein, wird aber nicht verwendet.
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onClick(View v) {
        StatistikFragment statistikFragment = new StatistikFragment();
        FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.replace(R.id.main_fragment_container, statistikFragment, "DETAILS");
        fragTransaction.commit();

    }
}

