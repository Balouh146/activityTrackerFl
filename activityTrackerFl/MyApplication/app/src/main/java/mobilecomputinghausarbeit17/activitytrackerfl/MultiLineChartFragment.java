package mobilecomputinghausarbeit17.activitytrackerfl;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;

/**
 * Created by Dennis on 05.01.2018.
 */

public class MultiLineChartFragment extends Fragment {
    private static final String	TAG	= "MultiLineChartFragment";

    private LineChart mLineChart;
    private DataToShow mData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView()");
        View view = inflater.inflate(R.layout.fragment_multi_line_chart, container, false);

        mLineChart = (LineChart) view.findViewById(R.id.multiple_line_chart);
        Description mDesc = new Description();
        mDesc.setText("Schrittaufteilung der letzten 24h");
        mLineChart.setDescription(mDesc);

        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        mLineChart.forceLayout();

        view.setMinimumHeight(mDisplay.getHeight());


        //mLineChart.setData(mData.getLineChartData());

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    public void setDataConnect(DataToShow data){
        mData = data;
    }

    public void invalidate(){
        mLineChart.setData(mData.getLineChartData());
        mLineChart.invalidate();
    }

    public void setData(int count, int range){

    }

}
