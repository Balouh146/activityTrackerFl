package mobilecomputinghausarbeit17.activitytrackerfl;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;

/**
 * Created by Dennis on 05.01.2018.
 */

public class PieChartFragment extends Fragment {
    private static final String	TAG	= "PieChart";

    private PieChart mPieChart;
    private DataToShow mData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView()");
        View view = inflater.inflate(R.layout.fragment_pie_chart, container, false);

        mPieChart = (PieChart) view.findViewById(R.id.pie_chart);

        Description mDesc = new Description();
        mDesc.setText("Schrittaufteilung der letzten 24h");
        mPieChart.setDescription(mDesc);

        mPieChart.setData(mData.getPieChartData());

        Display mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        mPieChart.forceLayout();
        view.setMinimumHeight(mDisplay.getHeight());

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    public void setDataConnect(DataToShow data){
        mData = data;
    }

    public void invalidate(){
        mPieChart.setData(mData.getPieChartData());
        mPieChart.invalidate();
    }
}
