package mobilecomputinghausarbeit17.activitytrackerfl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;



import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mirco on 04.12.2017.
 * modified by Steffen Vogel
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    //Für die Berechnugn des Tages
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    // Database Version
    private static final String DEBUG_TAG = "DatabaseHandler";

    private static final int DATABASE_VERSION = 17;
    // Database Name
    private static final String DATABASE_NAME ="ActivitTrackerDB.db";
    // Contacts table name
    private static final String TABLE_STEPS = "StepCounterTable";
    // Shops Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_STEPS = "step";
    private static final String KEY_TOTAL_STEPS = "totalCounter";
    private static final String KEY_TIMESTAMP = "timeStamp";

    //Tabelle für die TagesStatistiken  (Wie viele Minuten Laufen und Gehen...)
    private static final String TABLE_DAYSTATS = "DayStatisticTable";
    private static final String STATS_ID = "id";
    private static final String STATS_DATE = "date";
    private static final String KEY_WALK = "walking";
    private static final String KEY_RUN = "running";

    //Tabelle für die Tracks (von wann bis wann laufen, joggen etc... )
    private static final String TABLE_TRACKS = "TrackTable";
    private static final String TRACK_ID = "id";
    private static final String TRACK_DATE ="date";
    private static final String TRACK_MODE ="mode";
    private static final String TRACK_START ="start";
    private static final String TRACK_END = "end";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    //IF
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_STEPS + "("+
                KEY_ID +" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+
                KEY_STEPS + " INTEGER," +
                KEY_TOTAL_STEPS +" INTEGER,"+
                KEY_TIMESTAMP + " INTEGER" +
                ")";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_STATISTIC_TABLE ="CREATE TABLE " + TABLE_DAYSTATS + "(" +
                STATS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                STATS_DATE + " TEXT," +
                KEY_WALK + " INTEGER," +
                KEY_RUN + " INTEGER" +
                ")";
        db.execSQL(CREATE_STATISTIC_TABLE);

        String CREATE_TRACKS_TABLE ="CREATE TABLE " + TABLE_TRACKS + "(" +
                TRACK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                TRACK_DATE + " TEXT," +
                TRACK_MODE + " TEXT," +
                TRACK_START + " TEXT," +
                TRACK_END + " TEXT" +
                ")";
        db.execSQL(CREATE_TRACKS_TABLE);


        putSomeTestValues(db);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STEPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DAYSTATS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACKS);

        // Creating tables again
        onCreate(db);
    }

    // Adding total amout of steps to database
    public void AddStep(String step){

        double newstep = Double.parseDouble(step);
        int oldData = GetStepData();



        int i2 = (int)newstep - oldData;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_STEPS, i2);
        values.put(KEY_TOTAL_STEPS,oldData);
        values.put(KEY_TIMESTAMP, System.currentTimeMillis());

        //Tag Heute
        long currentDay = System.currentTimeMillis();
        Date myDay = new Date();
        myDay.setTime(currentDay);
        String formatDate = dateFormat.format(myDay);

        //Tag aus Datenbank
        long dbCurrentDay = LastTimeStamp();
        Date myDay2 = new Date();
        myDay2.setTime(dbCurrentDay);
        String formatDate2 = dateFormat.format(myDay2);
        if(!formatDate.equals(formatDate2)){
            //Einfach erstmal 0 setzen
            values.put(KEY_STEPS, 0);
            //aktuellen werte aus dem Step Counter einlesen
            values.put(KEY_TOTAL_STEPS,newstep);
            //Aktuelle systemzeit rein packen
            values.put(KEY_TIMESTAMP, System.currentTimeMillis());
            db.insert(TABLE_STEPS, null, values);
        }


        //Wenn nichts in der Datenbank ist einfach mal was erstellen
        //Wird nachher noch gebraucht für stunden wechsel
        if(oldData == 0){
            //Einfach erstmal 0 setzen
            values.put(KEY_STEPS, 0);
            //aktuellen werte aus dem Step Counter einlesen
            values.put(KEY_TOTAL_STEPS,newstep);
            //Aktuelle systemzeit rein packen
            values.put(KEY_TIMESTAMP, System.currentTimeMillis());
            db.insert(TABLE_STEPS, null, values);
        }

        //Ganz komisch hier muss irgendwie ein vom typ String[] sein
        //einfach so machen ist nicht fragen
        db.update(TABLE_STEPS, values, KEY_ID + "= ?",  new String[]{String.valueOf(LastId())});
        //Close Database connections
        db.close();
    }

    //get all steps from the database
    public int GetStepData(){
        String selectQuery = "SELECT * FROM " + TABLE_STEPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor != null && cursor.getCount()>0 ){
            cursor.moveToLast();
            Log.wtf("Database_Output","Cursor Daten ID: " + cursor.getString(0));
            Log.wtf("Database_Output","Cursor Daten: Steps" + cursor.getString(1));
            Log.wtf("Database_Output","Cursor Daten: Total Steps" + cursor.getString(2));
            Log.wtf("Database_Output","Cursor Daten: Timestamp" + cursor.getString(3));
            int i = Integer.parseInt(cursor.getString(2));

            return i;
        }

        return 0;
    }

    public int LastId(){
        String selectQuery = "SELECT * FROM " + TABLE_STEPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null && cursor.getCount()>0 ){
            cursor.moveToLast();
            Log.wtf("Database_Output","Cursor Daten ID: " + cursor.getString(0));
            Log.wtf("Database_Output","Cursor Daten: Steps" + cursor.getString(1));
            Log.wtf("Database_Output","Cursor Daten: Total Steps" + cursor.getString(2));
            Log.wtf("Database_Output","Cursor Daten: Timestamp" + cursor.getString(3));
            int i = Integer.parseInt(cursor.getString(0));
            return i;
        }
        return 0;
    }

    public long LastTimeStamp(){
        String selectQuery = "SELECT * FROM " + TABLE_STEPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null && cursor.getCount()>0 ){
            cursor.moveToLast();
            Log.wtf("Database_Output","Cursor Daten ID: " + cursor.getString(0));
            Log.wtf("Database_Output","Cursor Daten: Steps" + cursor.getString(1));
            Log.wtf("Database_Output","Cursor Daten: Total Steps" + cursor.getString(2));
            Log.wtf("Database_Output","Cursor Daten: Timestamp" + cursor.getString(3));
            long i =  Long.parseLong(cursor.getString(3));
            return i;
        }
        return 0;
    }
    public int GetSingleStep(){
        String selectQuery = "SELECT * FROM " + TABLE_STEPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null && cursor.getCount()>0 ){
            cursor.moveToLast();
            Log.wtf("Database_Output","Cursor Daten ID: " + cursor.getString(0));
            Log.wtf("Database_Output","Cursor Daten: Steps" + cursor.getString(1));
            Log.wtf("Database_Output","Cursor Daten: Total Steps" + cursor.getString(2));
            Log.wtf("Database_Output","Cursor Daten: Timestamp" + cursor.getString(3));
            int i = Integer.parseInt(cursor.getString(1));
            return i;
        }
        return 0;
    }

    /* NEUE FUNKTIONEN FÜR DIE STATISTIK TABELLE ****************************************/


    public void addTrack(String timeStamp, String mode, String startTime, String endTime) {
        Log.d(DEBUG_TAG,"Track in die Datenbank einfügen");
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TRACK_DATE,timeStamp);
        values.put(TRACK_MODE,mode);
        values.put(TRACK_START,startTime);
        values.put(TRACK_END,endTime);

        database.insert(TABLE_TRACKS,null,values);
        database.close();
    }

    //Liefert einen Array mit einem Stringtext der die Daten des Tracks enthält.
    //TODO: Vllt hier auch modular die Daten einzeln ausgeben, z.bsp in einer Map...
    public String[] getTracks(String timeStamp) {
        Log.d(DEBUG_TAG,"Tracks aus der Datenbank holen zu: "+timeStamp);
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery ="SELECT * FROM "+TABLE_TRACKS + " WHERE " + TRACK_DATE + " = \""+timeStamp +"\"";
        Cursor cursor = db.rawQuery(selectQuery,null);
        String StringArray[] = new String[cursor.getCount()];

        Log.d(DEBUG_TAG,"cursor.getCount(): "+cursor.getCount());
        if(cursor != null && cursor.getCount() > 0 ) {
            cursor.moveToFirst();
            for(int i= 0; i < cursor.getCount(); i++) {
                String mode = cursor.getString(2);
                String start = cursor.getString(3);
                String end = cursor.getString(4);

                String ausgabe = ""+mode+" am: "+start+" Ende:  "+end;
                StringArray[i] = ausgabe;
                cursor.moveToNext();
            }
        }
        return StringArray;
    }





    public void addToStatistic(String timeStamp, String column,int valueToAdd) {
        Log.d(DEBUG_TAG,"Wert zur Statistik hinzufügen");
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if(column == "KEY_WALK") {values.put(KEY_WALK,getStatData(timeStamp,"KEY_WALK")+valueToAdd);}
        else if(column == "KEY_RUN") {values.put(KEY_RUN,getStatData(timeStamp,"KEY_RUN")+valueToAdd);}

        int numberOfRowsAffected = database.update(TABLE_DAYSTATS,values,STATS_DATE+" = ?",new String[]{timeStamp});

        if(numberOfRowsAffected == 0 ) { //Falls es zu diesem Tag noch keinen Eintrag gibt, Eintrag einfügen
            Log.d(DEBUG_TAG,"Neue Tagesstatistik wird angelegt");
            ContentValues newValues = new ContentValues();
            newValues.put(STATS_DATE,timeStamp);
            newValues.put(KEY_WALK,0);
            newValues.put(KEY_RUN,0);

            database.insert(TABLE_DAYSTATS,null,newValues);
        }
        database.close();
    }




    //Holt den Wert aus der aktuellen Spalte zu einem übergebenen TimeStamp (zbsp Minuten Laufen am 30.12.2017)
    public int getStatData(String timeStamp, String column) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_DAYSTATS + " WHERE " + STATS_DATE + " = \""+timeStamp+"\"";
        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor != null && cursor.getCount() > 0 ) {
            cursor.moveToLast();
            if(column == "KEY_WALK") {return cursor.getInt(2);}
            if(column == "KEY_RUN") {return cursor.getInt(3);}
        }
        return 0;
    }


    //Nur Zu Testzwecken!!! Hier ein paar Einträge in die Datenbank schreiben um die Funktion der UI zu testen et... //
    public void putSomeTestValues(SQLiteDatabase db) {
        ContentValues newValues = new ContentValues();
        newValues.put(STATS_DATE,"07/01/2018");
        newValues.put(KEY_WALK,30);
        newValues.put(KEY_RUN,40);
        db.insert(TABLE_DAYSTATS,null,newValues);

        ContentValues values2 = new ContentValues();
        values2.put(STATS_DATE,"06/01/2018");
        values2.put(KEY_WALK,60);
        values2.put(KEY_RUN,40);
        db.insert(TABLE_DAYSTATS,null,values2);

        ContentValues values3 = new ContentValues();
        values3.put(STATS_DATE,"05/12/2017");
        values3.put(KEY_WALK,20);
        values3.put(KEY_RUN,15);
        db.insert(TABLE_DAYSTATS,null,values3);

        ContentValues values4 = new ContentValues();
        values4.put(STATS_DATE,"04/12/2017");
        values4.put(KEY_WALK,90);
        values4.put(KEY_RUN,100);
        db.insert(TABLE_DAYSTATS,null,values4);

    }
}
