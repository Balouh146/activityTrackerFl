package mobilecomputinghausarbeit17.activitytrackerfl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;


public class MainActivity extends AppCompatActivity {
    private static final String DEBUG_TAG = "MainActivity";

    //Konstanzen für die Auswahl des aktiven Fragments
    private static final String ACTIVE_FRAGMENT_MAIN = "DETAILS";
    private static final String ACTIVE_STATISTIK_FRAGMENT = "STATISTIC_FRAGMENT";
    private static final String ACTIVE_MULTI_LINE_FRAGMENT = "MULTI_LINE_FRAGMENT";
    private static final String ACTIVE_PIE_CHART_FRAGMENT = "PIE_CHART_FRAGMENT";

    //Instanzen der Fragments:
    private MainFragment mainFragment;
    private StatistikFragment statistikFragment;
    private MultiLineChartFragment multiLineChartFragment;
    private PieChartFragment pieChartFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mainFragment = new MainFragment();
        statistikFragment = new StatistikFragment();
        multiLineChartFragment = new MultiLineChartFragment();


        //StandardFragment ist das MainFragment (Ansicht des Aktuellen Tages..
        setMainFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Aufblase des Menüs wenn Elemente vorhanden sind
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Auswahl des anzuzeigenden Fragments
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d(DEBUG_TAG, "onOptionsItemSelected( " + item.toString() + " )");

        if(id == R.id.fragment_main) {
            //Ist das Fragment bereits aktiv
            if ( getFragmentManager().findFragmentByTag(ACTIVE_FRAGMENT_MAIN)
                    != null ) {
                Log.d(DEBUG_TAG, "onOptionsItemSelected: Main Fragment ist aktiv");
                return true;
            } else {
                setMainFragment();
            }
            return true;
        }

        if(id == R.id.statistic_fragment) {
            //Ist das Fragment bereits aktiv
            if ( getFragmentManager().findFragmentByTag(ACTIVE_STATISTIK_FRAGMENT)
                    != null ) {
                Log.d(DEBUG_TAG, "onOptionsItemSelected: Statistik Fragment ist aktiv");
                return true;
            } else {
                setStatistikFragment();
            }
            return true;
        }

        if(id == R.id.fragment_multi_line_chart) {
            //Ist das Fragment bereits aktiv
            if ( getFragmentManager().findFragmentByTag(ACTIVE_MULTI_LINE_FRAGMENT)
                    != null ) {
                Log.d(DEBUG_TAG, "onOptionsItemSelected: Main Fragment ist aktiv");
                return true;
            } else {
                setMultiLineChartFrag();
            }
            return true;
        }

        if(id == R.id.fragment_pie_chart) {
            //Ist das Fragment bereits aktiv
            if ( getFragmentManager().findFragmentByTag(ACTIVE_PIE_CHART_FRAGMENT)
                    != null ) {
                Log.d(DEBUG_TAG, "onOptionsItemSelected: Main Fragment ist aktiv");
                return true;
            } else {
                setMultiLineChartFrag();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Das Main Fragment dem Container zuweisen
    private void setMainFragment(){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, mainFragment,
                        ACTIVE_FRAGMENT_MAIN)
                .commit();
        //führe Änderungen aus
        getFragmentManager().executePendingTransactions();
    }


    //Das Statistik Fragment dem Container zuweisen
    private void setStatistikFragment(){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, statistikFragment,
                        ACTIVE_STATISTIK_FRAGMENT)
                .commit();
        //führe Änderungen aus
        getFragmentManager().executePendingTransactions();
    }

    //Das Multi Line Chart Fragment dem Container zuweisen
    private void setMultiLineChartFrag(){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, multiLineChartFragment,
                        ACTIVE_MULTI_LINE_FRAGMENT)
                .commit();
        //führe Änderungen aus
        getFragmentManager().executePendingTransactions();
    }

    //Das Multi Line Chart Fragment dem Container zuweisen
    private void setPieChartFragment(){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_fragment_container, pieChartFragment,
                        ACTIVE_PIE_CHART_FRAGMENT)
                .commit();
        //führe Änderungen aus
        getFragmentManager().executePendingTransactions();
    }
} //Ende der Klasse



