package mobilecomputinghausarbeit17.activitytrackerfl;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;

/**
 * Created by Dennis on 05.01.2018.
 */

public class DataToShow {
    private static final String	TAG	= "DataToShow";
    private static final int THRESHOLD = 125;
    private DatabaseHandler db;
    private int walkingStepsCount;
    private int runningStepsCount;
    private ArrayList<int[]> mData;


    public DataToShow() {
        walkingStepsCount = 0;
        runningStepsCount = 0;
    }

    public void setDb(DatabaseHandler db){
        this.db = db;
        mData = new ArrayList<int[]>();
    }

    public void calculation(){
        int position = 0;
        int steps = 0;
        int[] data = {1, 2, 3};
        ArrayList<int[]> allData = new ArrayList<int[]>();
        // TODO anpassen der Daten bank abfrage
        // data = db.getData(position);
        while(data != null) {
            allData.add(position, data);
            try{
                steps = data[1];
                Log.v(TAG,"Data[1]: " + data[1]);
            } catch(Exception e) {
                Log.v(TAG,"Exception thrown: " + e.getMessage());
            }
            if(steps < THRESHOLD){
                walkingStepsCount = walkingStepsCount + steps;
            } else {
                runningStepsCount = runningStepsCount + steps;
            }
            // TODO anpassen der Daten bank abfrage
            //data = db.getData(position);
            position++;
        }
        mData = allData;
        Log.i(TAG,"calculation(): Schritte gegangen: " + walkingStepsCount);
    }

    public PieData getPieChartData(){
        ArrayList<PieEntry> entries = new ArrayList<>();

        entries.add(new PieEntry(walkingStepsCount,"Gehen"));
        entries.add(new PieEntry(runningStepsCount,"Laufen"));

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSelectionShift(2f);
        dataSet.setSliceSpace(2f);
        dataSet.setColors(RED, GREEN);
        PieData data = new PieData(dataSet);
        return data;
    }

    public LineData getLineChartData(){
        ArrayList<Entry> yValues1 = new ArrayList<Entry>();
        ArrayList<Entry> yValues2 = new ArrayList<Entry>();
        //ArrayList<Entry> yValues3 = new ArrayList<Entry>();
        for(int i = 0; i < mData.size(); i++)
        {
            yValues1.add(new Entry((float)i, (float)mData.get(i)[1]));
            yValues2.add(new Entry((float)i, (float)mData.get(i)[2]));
            //yValues3.add(new Entry((float)i, (float)mData.get(i)[3]));


        }

        LineDataSet set1, set2, set3;

        set1 = new LineDataSet(yValues1, "Gehen");
        set1.setColor(RED);
        set1.setCircleColor(RED);

        set2 = new LineDataSet(yValues2, "Laufen");
        set2.setColor(GREEN);
        set2.setCircleColor(GREEN);

        //set3 = new LineDataSet(yValues3, "Data Set3");

        //LineData data = new LineData(set1, set2, set3);
        LineData data = new LineData(set1, set2);

        return data;
    }
}
