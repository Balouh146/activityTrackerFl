package mobilecomputinghausarbeit17.activitytrackerfl;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by Steffen on 27.12.2017.
 */

public class KreisDiagramm extends View {

    private static final String TAG = "Kreisdiagramm";
    private Paint myPaint;
    private double value1 =0 ;
    private String label1;
    private double value2 = 0;
    private String label2;


    //Konstruktoren für das Diagramm View
    public KreisDiagramm(Context context) {
        super(context);
    }

    public KreisDiagramm(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    //StandardKonstruktor beim Anlegen in der XML Datei
    public KreisDiagramm(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.v(TAG, "MyView(Context context, AttributeSet attrs):  ");
        this.myPaint = new Paint();
    }

    @Override  // onDraw Methode: Aktualisiert das Diagramm.
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int oColor1 = Color.parseColor("#C9028A");
        int oColor2 = Color.parseColor("#5303F4");

        int textSize = 70;
        //Berechnung der Kleinsten und Längsten Seite. Von der Höhe wird die 2 Fache schriftgröße abgezogen um Platz für die Beschriftung zu haben.
        //Berechnung ist Notwendig damit der Kreis als Kreis und nicht als Oval erscheint, sowie zum Zentrieren des Kreises in der MItte des View-Objektes.
        int smallestSite = Math.min(this.getWidth(),this.getHeight());
        int longestSite = Math.max(this.getWidth(),this.getHeight());

        int left = (longestSite-smallestSite)/2; //Zentrieren des Kreises/Rechtecks
        RectF box = new RectF(left,2,left+smallestSite,smallestSite); //Für die Winkelzeichnung

        //Berechnung der Winkel (als Verhältnis der Summe)
        double summe = this.value1 + this.value2 ;
        double sweep1 = (this.value1/summe) * 360;
        double sweep2 = (this.value2/summe) * 360;

        //Zeichnen der Winkel und der Beschriftungen
        myPaint.setStyle(Paint.Style.FILL);
        myPaint.setStrokeWidth(30); //
        myPaint.setColor(Color.LTGRAY);
        myPaint.setTextSize(70.0f);

        //Grauen Default Kreis zeichnen (falls keine Werte vorhanden sind..
        canvas.drawArc(box,0.0F,360,true,myPaint);
        myPaint.setColor(oColor1);
        canvas.drawArc(box,270.0F,(float) sweep1,true,myPaint);
      //  canvas.drawText(""+(int)this.value1+" Minuten "+this.label1,0,this.getHeight()-myPaint.getTextSize(),myPaint);
        myPaint.setColor(oColor2);
        canvas.drawArc(box,270.0F+(float)sweep1,(float)sweep2,true,myPaint);
      //  canvas.drawText(""+(int)this.value2+" Minuten "+this.label2,0,this.getHeight()-2*myPaint.getTextSize(),myPaint);
    }

    //Mit dieser Methode soll von aussen heraus das Diagramm gezeichnet werden. Es können die beiden darzustellenden Werte
    //und die Beschriftung übergeben werden. //TODO: falls mehr als 2 Aktivitäten im Diagramm angezeigt werden, analog den COde erweitern oder mit Arrays arbeiten.
    public void drawDiagramm(double value1, double value2) {
        this.value1 = value1;
        this.value2 = value2;
        this.invalidate();
    }
}
