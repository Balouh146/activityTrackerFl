package mobilecomputinghausarbeit17.activitytrackerfl;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Steffen on 01.01.2018.
 */

public class StatistikFragment extends Fragment implements View.OnClickListener {

    private static final String	DEBUG_TAG	= "StatistikFragment";

    private long unixTimeStamp;   //Zum Hoch Runterzählen der Tage
    private Date formatdate = new Date(); //Zum Setzen des Tages als Dateobjekt (Nötig für Formatierung)
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private String StringDateFormat; //Zum Speichern des gewählten Datums im Formatstring
    private KreisDiagramm kreisDiagramm; //Zum Anzeigen des Diagramms
    private TextView textTimestamp; //Zur Anzeige des Ausgewählten Datums
    public TextView walkingView;
    public TextView runningView;
    private TextView textViewTracks;
    private DatabaseHandler db;

    public StatistikFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_statistik, container, false);

        Button todayButton = (Button) view.findViewById(R.id.today);
        Button leftButton = (Button) view.findViewById(R.id.back);
        Button rightButton = (Button) view.findViewById(R.id.right);
        kreisDiagramm = (KreisDiagramm) view.findViewById(R.id.KreisDiagramm);
        textTimestamp = (TextView) view.findViewById(R.id.timestampText);
        textViewTracks = (TextView) view.findViewById(R.id.textViewTracks);
        walkingView = (TextView) view.findViewById(R.id.walkingView);
        runningView = (TextView) view.findViewById(R.id.runningView);
        textViewTracks.setMovementMethod(new ScrollingMovementMethod());
        todayButton.setOnClickListener(this);
        leftButton.setOnClickListener(this);
        rightButton.setOnClickListener(this);

        db = new DatabaseHandler(getActivity());

        Date date = new Date();
        unixTimeStamp = date.getTime();
        updateFragment();
        return view;
    }

    @Override
    public void onClick(View v) {
        Log.d("Test","Irgendwas geklickt");
        switch(v.getId()){
            //TODAY wechselt das Fragment zur LiveAnsicht (Also dem Heutigen Tag mit aktueller Aufzeichnung
            case R.id.today :
                Log.d("Test","Fragment soll wechseln");
                MainFragment mainFragment = new MainFragment();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.main_fragment_container, mainFragment, "DETAILS");
                fragTransaction.commit();
                break;

            case R.id.back :
                Log.d(DEBUG_TAG,"Einen Tag zurückspringen");
                unixTimeStamp -= 86400000;  //Einen Tag "abziehen";
                formatdate.setTime(unixTimeStamp); //
                updateFragment();
                break;

            case R.id.right :
                Log.d(DEBUG_TAG,"Einen Tag vorspringen");
                unixTimeStamp += 86400000; //Einen Tag "addieren";
                updateFragment();
                break;
        }
    }

    //Aktualisiert das Fragment mit den Daten aus der Datenbank zum Zugehörigen Datum (dem im Feld Unixtimestamp gespeicherten wert)
    private void updateFragment() {
        formatdate.setTime(unixTimeStamp);  //UnixZeit in ein Date Objekt speichern..
        StringDateFormat = dateFormat.format(formatdate); //Dateobjekt als String formatieren
        //Werte fürs Kreisdiagramm laden und zeichnen
        int walkMinutes = db.getStatData(StringDateFormat,"KEY_WALK");
        int jogMinutes = db.getStatData(StringDateFormat,"KEY_RUN");
        kreisDiagramm.drawDiagramm(jogMinutes,walkMinutes);
        textTimestamp.setText(StringDateFormat);


        if(walkMinutes == 0){
            walkingView.setText("Keine Daten vorhanden");
        }
        else{
            walkingView.setText(String.valueOf(walkMinutes) + " Minute(n)");
        }
        if(jogMinutes == 0){
            runningView.setText("Keine Daten vorhanden");
        } else{
            runningView.setText(String.valueOf(jogMinutes) + " Minuten(n)");
        }

        //Aktuelle Tracks aus der Datenbank holen und in die Oberfläche eintragen.
        String[] ResultArray = db.getTracks(StringDateFormat);
        String textforTextView = "";
        for(int i = 0; i < ResultArray.length;i++) {
            textforTextView += ResultArray[i] + "\n";
        }
        textViewTracks.setText(textforTextView);
    }
}
